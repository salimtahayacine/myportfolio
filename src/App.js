import Login from "./components/Login";
import NavBar from "./components/NavBar"
import {BrowserRouter as Router ,Route,Routes} from 'react-router-dom';
import Home from "./components/Home";
import About from "./components/About";
import Footer from "./components/Footer";

function App() {
  return (
    <Router>
      <div>
       <NavBar/>
      </div>
      <Routes>
        <Route  path='/' element ={<Home />} ></Route>
        <Route path="/login" element={<Login />}></Route>
        <Route path="/about" element={<About />}></Route>
        <Route path="/footer" element={<Footer />}></Route>
      </Routes>
      <Footer/>
    </Router>
    
  );
}

export default App;
