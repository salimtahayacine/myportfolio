import React from 'react'

function Items({Links,title}) {
  return (
    <div className=''>
        <ul>
            <h1 className='mb-1 font-semibold text-white'>{title}</h1>
            {Links.map( (Link) => (
                <li key={Link.name}>
                    <a className='text-gray-400 hover:text-teal-400 duration-300 text-sm cursor-pointer' href={Link.Link}>{Link.name}</a>
                </li>
            ))}
        </ul>
    </div>
  )
}

export default Items