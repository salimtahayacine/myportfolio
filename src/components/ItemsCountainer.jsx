import React from 'react';
import Items from './Items';
import {RESOURCES,PRODUCTS,COMPANY,SUPPORT} from './MenuData';

function ItemsCountainer() {
  return (
    <div className='grid grid-cols-1 sm:grid-cols-3 lg:grid-cols-4 gap-6 sm:px-8 px-5 py-16 bg-slate-600 text-white'>
        <Items Links={PRODUCTS} title="PRODUCTS"/>
        <Items Links={RESOURCES} title="RESOURCES"/>
        <Items Links={COMPANY} title="COMPANY"/>
        <Items Links={SUPPORT} title="SUPPORT"/>
    </div>
  )
}

export default ItemsCountainer