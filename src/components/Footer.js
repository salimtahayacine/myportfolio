import React from 'react';
import ItemsCountainer from './ItemsCountainer';
//import { Link } from 'react-router-dom';
import SocialIcons from "./SocialIcons";
import { Icons } from "./MenuData";


function Footer() {
  return (
       <footer className='bg-gray-700 text-white absolute inset-x-0 bottom-0 '>
        <div className='md:flex md:justify-between md:items-center sm:px-12 px-4 py-8  h-16 bg-[#ffffff19] '>

        </div>
                <div className='md:flex md:justify-between md:items-center sm:px-12 px-4 py-8  h-16 bg-[#ffffff19] '>
                  <h4 className='text-white lg:text-4xl text-3xl md:mb-0 mb-6 lg:leading-normal font-semibold md:w-2/5'><span className='text-teal-400'>Free</span> until you're ready to launche</h4>
                  <div>
                    <input type="text" placeholder="Enter Your ph.no"
                     className='text-gray-800 sm:w-72 w-full sm:mr-5 mr-1 lg:mb-0 mb-4 py-2.5 rounded px-2 focus:outline-none'/>
                     <button className='md:w-auto w-full bg-teal-400 hover:bg-teal-500 duration-300 px-5 py-2.5 font-[Poppins] rounded-md text-white'>Request Code</button>
                  </div>
                </div>
                <ItemsCountainer/>
                <div className='grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-10 text-center
                 pt-2 text-gray-400 text-sm pb-8'>
                    <span className='text-white'>© 2022 Appy. All rights reserved by Stronglover.</span>
                    <span className='text-white'>Terms · Privacy Policy</span>
                    <SocialIcons Icons={Icons} />
                </div>
       </footer>);
}

export default Footer