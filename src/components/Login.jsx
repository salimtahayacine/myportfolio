import React from 'react';
//import loginbg from './assets/';

export default function () {
  return (
    <div className='grid grid-cols-1  h-screen w-full'>
        <div className='bg-gray-400 flex flex-col justify-center'>
            <form className=' max-w-[400px] w-full mx-auto bg-gray-900 p-8 px-8 rounded-lg shadow-lg shadow-gray-500'>
                <h2 className=' text-4xl text-white font-bold text-center'>Sign UP/ Sign IN</h2>
                <div className='flex flex-col text-gray-400 py-2'>
                <label name="text">User Name</label>
                <input  className=' rounded-lg bg-gray-600 mt-2 p-2 focus:border-blue-500 focus:bg-gray-800 focus:outline-none' type="text" required/>
               </div>
               <div className='flex flex-col text-gray-400 py-2'>
                <label name="email">E-mail</label>
                <input className=' rounded-lg bg-gray-600 mt-2 p-2 focus:border-blue-500 focus:bg-gray-800 focus: outline-none' type="email" required/>
               </div>
               <div className='flex flex-col text-gray-400 py-2'>
                <label name="password">Password</label>
                <input className=' rounded-lg bg-gray-600 mt-2 p-2 focus:border-blue-500 focus:bg-gray-800 focus: outline-none' type="password" required/>
               </div>
               <div className=' flex justify-between text-gray-400 py-2'>
                    <p className='flex items-center'> <input className='mr-2' type="checkbox" required/> Remember Me</p>
                    <a href="/">forgot password</a> <br />
               </div>
               <button className=' rounded-lg w-full my-5 py-2  bg-teal-400 shadow-lg hover:shadow-teal-500/50'>Sign In</button>
            </form>
        </div>
    </div>
  )
}
